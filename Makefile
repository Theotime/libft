# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: revers <revers@student.42.fr>              +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2013/11/23 16:38:49 by triviere          #+#    #+#              #
#    Updated: 2015/12/21 15:49:52 by triviere         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = libft.a
CMD_CP = gcc

INCLUDES=-Iincludes

FLAG = -Wall -Werror -Wextra -g3 $(INCLUDES)

DIR_SRC = srcs
DIR_OBJ = objs
DIR_INCLUDES = includes/
DIR_OTHER = ./
DEPS = includes/ft_lst.h includes/ft_sys.h includes/get_next_line.h \
	includes/libft.h

FILE_PGM = main.c

SRC = $(shell find $(DIR_SRC) -type f)
PRE_COMP_PGM = $(FILE_PGM:.c=.o)
OBJ = $(patsubst $(DIR_SRC)/%,$(DIR_OBJ)/%, $(SRC:.c=.o))

all: $(NAME)

$(DIR_OBJ)/%.o: $(DIR_SRC)/%.c $(DEPS)
	@mkdir -p $(dir $@)
	$(CMD_CP) -o $@ -c $< $(FLAG)

$(NAME): $(OBJ)
	ar rc $(NAME) $(OBJ)
	ranlib $(NAME)

clean:
	rm -rf $(DIR_OBJ)

fclean: clean
	rm -f $(NAME)

re: fclean all

.PHONY : all clean fclean re
