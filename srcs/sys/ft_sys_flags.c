/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sys_flags.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: revers <revers@student.42.fr>              +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/21 16:10:37 by triviere          #+#    #+#             */
/*   Updated: 2015/12/24 00:53:23 by revers           ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

void		ft_sys_init_flags(t_sys *sys)
{
	sys->flags = ft_lst_init();
	sys->add_flag('i', "interactive", 0, 0);
	sys->add_flag('v', "verbose", 0, 0);
	sys->add_flag('d', "debug", 0, 0);
}

void		ft_sys_add_flag(char sname, char *lname, int min, int max)
{
	t_sys		*sys;
	t_flag		*flag;

	sys = ft_get_sys();
	flag = malloc(sizeof(t_flag));
	ft_bzero((void*)flag, sizeof(t_flag));
	flag->sname = sname;
	flag->lname = lname;
	flag->min = min;
	flag->max = max;
	flag->args = ft_lst_init();
	ft_lst_push(sys->flags, ft_el_init((void*)flag));
}

t_el		*ft_sys_flag_get_lflag(char *flag)
{
	t_sys		*sys;
	t_el		*cur;

	sys = ft_get_sys();
	cur = sys->flags->start;
	while (cur)
	{
		if (!ft_strcmp(((t_flag*)cur->data)->lname, flag))
			return (cur);
		cur = cur->next;
	}
	return (NULL);
}

t_el		*ft_sys_flag_get_sflag(char flag)
{
	t_sys		*sys;
	t_el		*cur;

	sys = ft_get_sys();
	cur = sys->flags->start;
	while (cur)
	{
		if (((t_flag*)cur->data)->sname == flag)
			return (cur);
		cur = cur->next;
	}
	return (NULL);
}

void		ft_sys_bag_flag(char *flag)
{
	t_sys		*sys;
	char		*message;

	sys = ft_get_sys();
	message = ft_strjoin("Bad option : ", flag);
	sys->error(message);
	sys->display_help();
	exit(1);
}
